import xbmc
import xbmcgui
import xbmcplugin

from resources.lib.const import STRINGS, ROUTE, ENDPOINT
from resources.lib.kodilogging import logger
from resources.lib.utils.kodiutils import go_to_plugin_url, replace_plugin_url, set_resolved_url
from resources.lib.utils.url import Url


class Router:
    @staticmethod
    def go(route, **kwargs):
        logger.debug('Going to route: %s' % route)
        go_to_plugin_url(STRINGS.QUERY.format(route, Url.encode(kwargs)))

    @staticmethod
    def get_url(_route, **kwargs):
        return STRINGS.QUERY.format(_route, Url.encode(kwargs))

    @staticmethod
    def get_stream_url(media_id, root_parent_id):
        return Router.get_url(ROUTE.PROCESS_MEDIA_ITEM,
                              url=ENDPOINT.STREAMS.format(media_id=media_id),
                              media_id=media_id,
                              root_parent_id=root_parent_id)

    @staticmethod
    def get_url_api(route, url):
        return STRINGS.QUERY.format(route, Url.quote_plus(Router.encode_url(url)))

    @staticmethod
    def get_url_command(command):
        return STRINGS.QUERY.format(ROUTE.COMMAND, Url.encode({
            'command': command
        }))

    # After redirect it also resets KODI path history a.k.a "back" action takes you to root path
    @staticmethod
    def replace_route(route):
        logger.debug('Replacing path with {0}'.format(route))
        replace_plugin_url(route)

    @staticmethod
    def set_resolved_url(handle, li=xbmcgui.ListItem(path='Invalid URL')):
        set_resolved_url(handle, li)

    @staticmethod
    def play(handle, url, li=None):
        if handle == -1:
            xbmc.Player().play(url, li)
        else:
            router.set_resolved_url(handle, li)

    @staticmethod
    def encode_url(url):
        return Url.encode({
            'url': url
        })

    @staticmethod
    def exit():
        xbmc.executebuiltin("ActivateWindow(Home)")

    @staticmethod
    def get_media(media_type, render_type, url, media_url=ROUTE.GET_MEDIA, **kwargs):
        return router.get_url(media_url,
                              media_type=media_type,
                              render_type=render_type,
                              url=Url.quote_plus(url), **kwargs)



router = Router()
