from datetime import datetime, timedelta

import xbmcgui
import xbmcvfs

from resources.lib.api.api import API
from resources.lib.api.trakt_api import trakt, trakt_history_type_map
from resources.lib.const import COMMAND, LANG, DOWNLOAD_STATUS, SERVICE, DOWNLOAD_SERVICE_EVENT, HTTP_METHOD, \
    trakt_type_map, MEDIA_TYPE, STRINGS, ROUTE, RENDER_TYPE
from resources.lib.gui import InfoDialog
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.gui.text_renderer import DateRenderer
from resources.lib.kodilogging import logger
from resources.lib.routing.router import router
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import DB
from resources.lib.utils.addtolib import add_to_library, update_library
from resources.lib.utils.download import get_file_path
from resources.lib.utils.kodiutils import show_settings, get_string, get_log_file_path, read_log, clean_log, post_log, \
    show_log_dialog, refresh, show_settings_with_focus, busy_dialog, parse_date, get_time_offset, \
    daterange, datetime_to_str, decode_qs, is_youtube_url
from resources.lib.utils.speedtest import speed_test
from resources.lib.utils.url import Url
from resources.lib.utils.youtube import get_yt_video_url


class Command:
    def __init__(self, plugin_core):
        self._plugin_core = plugin_core

        self.command_map = {
            COMMAND.OPEN_SETTINGS: self.show_settings,
            COMMAND.SHOW_SETTINGS_WITH_FOCUS: self.show_settings_with_focus,
            COMMAND.SHOW_LOG_PATH: self.show_log_path,
            COMMAND.UPLOAD_LOG: self.upload_log,
            COMMAND.SET_PROVIDER_CREDENTIALS: self.set_provider_credentials,
            COMMAND.SET_SUBTITLES_CREDENTIALS: DialogRenderer.set_subtitles_credentials,
            COMMAND.REFRESH_PROVIDER_TOKEN: self._plugin_core.auth.refresh_provider_token,
            COMMAND.ADD_TO_LIBRARY: add_to_library,
            COMMAND.DELETE_HISTORY_ALL: self.delete_history_all,
            COMMAND.PIN: self.pin,
            COMMAND.UNPIN: self.unpin,
            COMMAND.HIDE_MENU_ITEM: self.hide_menu_item,
            COMMAND.SHOW_MENU_ITEM: self.show_menu_item,
            COMMAND.DELETE_HISTORY_ITEM: self.delete_history_item,
            COMMAND.DELETE_DOWNLOAD_ITEM: self.delete_download_item,
            COMMAND.CANCEL_DOWNLOAD_ITEM: self.cancel_download_item,
            COMMAND.RESUME_DOWNLOAD_ITEM: self.resume_download_item,
            COMMAND.PAUSE_DOWNLOAD_ITEM: self.pause_download_item,
            COMMAND.UPDATE_LIBRARY: update_library,
            COMMAND.SPEED_TEST: speed_test,
            COMMAND.CONVERT_HISTORY: self.convert_history,
            COMMAND.CLEAR_HISTORY: self.clear_history,
            COMMAND.SYNC_LOCAL_WITH_TRAKT: self.sync_local_history_with_trakt,
            COMMAND.CHOOSE_DATE: self.choose_date,
            COMMAND.CHOOSE_TIME: self.choose_time,
            COMMAND.SEARCH: self.search,
            COMMAND.INCREMENT_ENUM: self.increment_enum,
            COMMAND.PLAY_TRAILER: self.play_trailer,
            COMMAND.TRAKT_LIST_APPEND: trakt.list_append,
            COMMAND.TRAKT_LIST_CLONE: trakt.list_clone,
            COMMAND.TRAKT_LIST_DELETE: trakt.list_delete,  # trakt_renderer?
            COMMAND.TRAKT_LIST_LIKE: trakt.list_like,
            COMMAND.TRAKT_LIST_RENAME: trakt.list_rename,
            COMMAND.TRAKT_LIST_UNLIKE: trakt.list_unlike,
            COMMAND.TRAKT_UNFOLLOW: trakt.unfollow,
            COMMAND.TRAKT_LOGOUT: trakt.logout,
            COMMAND.TRAKT_MEDIA_MENU: trakt.media_menu,
            COMMAND.TRAKT_MARK_AS_WATCHED: self.mark_as_watched_trakt,
            COMMAND.TRAKT_MARK_AS_UNWATCHED: self.mark_as_unwatched_trakt,
            COMMAND.TRAKT_LOGIN: trakt.login
        }

    def __call__(self, command, *args, **kwargs):
        return self.command_map[command]

    @staticmethod
    def show_settings():
        show_settings()

    @staticmethod
    def show_settings_with_focus(id1, id2):
        show_settings_with_focus(int(id1), int(id2))

    @staticmethod
    def pin(dir_route, key):
        DB.PINNED.add(dir_route, decode_qs(key))
        refresh()

    @staticmethod
    def unpin(dir_route, key):
        DB.PINNED.delete(dir_route, decode_qs(key))
        refresh()

    @staticmethod
    def hide_menu_item(dir_route, key):
        DB.HIDDEN.add(dir_route, decode_qs(key))
        refresh()

    @staticmethod
    def show_menu_item(dir_route, key):
        DB.HIDDEN.delete(dir_route, decode_qs(key))
        refresh()

    @staticmethod
    def show_log_path():
        DialogRenderer.ok(get_string(LANG.LOG_FILE_PATH), get_log_file_path())

    @staticmethod
    def upload_log():
        success, data = read_log()
        if success:
            content = clean_log(data)
            success, response = post_log(content)
            if success:
                show_log_dialog(response)
            else:
                DialogRenderer.ok(get_string(LANG.LOG_FILE_PATH), 'Failed to upload log file: ' + response)
        else:
            DialogRenderer.ok(get_string(LANG.LOG_FILE_PATH), 'Failed to get log file: ' + data)

    def set_provider_credentials(self):
        self._plugin_core.auth.set_provider_credentials()

    @staticmethod
    def delete_history_item(media_id):
        DB.WATCH_HISTORY.delete(media_id=media_id)
        refresh()

    @staticmethod
    def delete_history_all():
        DB.WATCH_HISTORY.delete()
        InfoDialog(get_string(LANG.HISTORY_DELETED)).notify()

    @staticmethod
    def delete_download_item(dl_id):
        item = DB.DOWNLOAD.get(dl_id)
        DB.DOWNLOAD.delete(dl_id=dl_id)
        xbmcvfs.delete(get_file_path(item[1], item[3]))
        refresh()

    @staticmethod
    def cancel_download_item(dl_id):
        item = DB.DOWNLOAD.get(dl_id)
        if item[4] == DOWNLOAD_STATUS.PAUSED:
            xbmcvfs.delete(get_file_path(item[1], item[3]))
        DB.DOWNLOAD.cancel_download(dl_id=dl_id, status=DOWNLOAD_STATUS.CANCELLED)
        refresh()

    def resume_download_item(self, dl_id):
        DB.DOWNLOAD.set_status(dl_id, DOWNLOAD_STATUS.QUEUED)
        self._plugin_core.send_service_message(SERVICE.DOWNLOAD_SERVICE, DOWNLOAD_SERVICE_EVENT.ITEM_RESUMED,
                                               download_id=dl_id)
        refresh()

    @staticmethod
    def pause_download_item(dl_id):
        DB.DOWNLOAD.set_status(dl_id, DOWNLOAD_STATUS.PAUSED)
        refresh()

    @staticmethod
    def mark_as_watched_trakt(**kwargs):
        with busy_dialog():
            trakt.mark_as_watched(**kwargs)
            refresh()

    @staticmethod
    def mark_as_unwatched_trakt(**kwargs):
        with busy_dialog():
            trakt.mark_as_unwatched(**kwargs)
            refresh()

    def convert_history(self):
        items = DB.WATCH_HISTORY.get_all()
        api_res = self._plugin_core.get_watched(items, False)
        if api_res:
            to_update = []
            for item in items:
                local_id = item[0]
                api_item = next((i for i in api_res['data'] if i['_id'] == local_id), None)
                if api_item:
                    source = API.get_source(api_item)
                    info_labels = source.get('info_labels')
                    media_type = info_labels.get('mediatype')
                    to_update.append(dict(media_id=local_id, watched=item[1], media_type=media_type))
            DB.WATCH_HISTORY.update_many(to_update)

    @staticmethod
    def clear_history(media_type=None):
        logger.debug('Clearing history for type: %s', media_type)
        DB.WATCH_HISTORY.delete_type(media_type)
        refresh()

    def search(self, media_type=MEDIA_TYPE.ALL):
        self._plugin_core.directory_renderer.search(media_type)

    def sync_local_history_with_trakt(self):
        logger.debug('Syncing local Kodi watched history with Trakt.tv')
        with busy_dialog():
            if not DB.FILES.active:
                DialogRenderer.ok(get_string(LANG.UNSUPPORTED_DEVICE), get_string(LANG.UNSUPPORTED_DEVICE_INFO))
                return
            local_files = DB.FILES.get_all()
            ids = [{'media_id': Url.parse_qs(Url.unquote_plus(item[2]))['media_id'][0], 'item': item} for item in
                   local_files]
            trakt.sync_lists_items(['movies', 'episodes'])
            response = self._plugin_core.api.get_all_pages(HTTP_METHOD.GET,
                                                           API.FILTER.ids([i['media_id'] for i in ids], False))
            watched = {}
            for item in trakt.get_history():
                media_type = trakt_type_map.get(item.__class__.__name__.lower())
                trakt_id = item.get_key('trakt')
                if media_type not in watched:
                    watched[media_type] = {}
                if trakt_id not in watched[media_type]:
                    watched[media_type][trakt_id] = []
                watched[media_type][trakt_id].append(item.watched_at.replace(tzinfo=None) + get_time_offset())

            trakt_data = {
                'movies': [],
                "shows": [],
                "seasons": [],
                "episodes": []
            }
            added_items = 0
            for item in response.get('data'):
                source = API.get_source(item)
                services = source.get('services')
                trakt_id = services.get('trakt')
                if trakt_id:
                    info_labels = source['info_labels']
                    media_type = info_labels.get('mediatype')
                    if media_type == MEDIA_TYPE.EPISODE or media_type == MEDIA_TYPE.MOVIE:
                        local_kodi_file = next(i['item'] for i in ids if i['media_id'] == item['_id'])
                        trakt_item_dates = watched.get(media_type, {}).get(trakt_id)
                        local_kodi_file_date = local_kodi_file[4]
                        if local_kodi_file_date:
                            exists = False
                            kodi_date = parse_date(local_kodi_file_date)
                            if trakt_item_dates:
                                for trakt_date in trakt_item_dates:
                                    if abs((trakt_date - kodi_date).total_seconds()) < 60:
                                        exists = True
                                        break
                            if not exists:
                                trakt_type = trakt_history_type_map.get(media_type)

                                trakt_item = {
                                    "ids": {
                                        "trakt": int(trakt_id)
                                    },
                                    "watched_at": (kodi_date - get_time_offset()).isoformat()
                                }
                                trakt_data[trakt_type].append(trakt_item)
                                added_items += 1
            if added_items > 0:
                trakt.sync_history_add(trakt_data)

    @staticmethod
    def choose_date(min_date, max_date, selected_date):
        dates = []
        dates_f = []
        for d in daterange(parse_date(min_date).date(), parse_date(max_date).date()):
            dates_f.append(str(DateRenderer(d)))
            dates.append(d)
        selected_date = str(DateRenderer(parse_date(selected_date).date()))
        preselect = dates_f.index(selected_date)
        index = DialogRenderer.select(get_string(LANG.CHOOSE_DATE), dates_f, preselect=preselect)
        if index > -1:
            router.go(ROUTE.TV_STATIONS, selected_date=dates[index],
                      min_date=min_date, max_date=max_date)

    @staticmethod
    def choose_time(media_type, selected_date):
        now = datetime.now()
        selected_date = parse_date(selected_date)
        append_day = selected_date.day
        if now.hour < 5:
            append_day += 1
        now = now.replace(day=append_day, hour=now.hour, minute=0, second=0, microsecond=0)
        dates = []
        dates_f = []
        for i in range(5, 29, 1):
            if i > 23:
                d = selected_date.replace(day=selected_date.day, hour=i - 24, minute=0, second=0)
                d += timedelta(days=1)
            else:
                d = selected_date.replace(day=selected_date.day, hour=i, minute=0, second=0)
            dates.append(d)
            dates_f.append(datetime_to_str(d, STRINGS.TV_TIME))
        index = DialogRenderer.select(get_string(LANG.CHOOSE_TIME), dates_f, preselect=dates.index(now))
        if index > -1:
            selected_time = dates[index]
            selected_time = datetime_to_str(selected_time) + 'Z'
            router.go(router.get_media(MEDIA_TYPE.ALL,
                                       RENDER_TYPE.TV,
                                       API.FILTER.tv_program(media_type, datetime_to_str(selected_date, STRINGS.DATE), time=selected_time),
                                       date=selected_date, time=selected_time))

    @staticmethod
    def increment_enum(setting_name, reverse='0'):
        settings.increment_enum(setting_name)
        refresh()

    @staticmethod
    def play_trailer(video_url, handle=-1, title=None):
        logger.debug('Getting trailer URL %s' % video_url)
        if is_youtube_url(video_url):
            video_url = get_yt_video_url(video_url)
        if video_url:
            router.play(handle, video_url, xbmcgui.ListItem(path=video_url, label=decode_qs(title) if title else None))
            return True
        else:
            DialogRenderer.ok(get_string(LANG.MISSING_STREAM_TITLE), get_string(LANG.STREAM_NOT_AVAILABLE))
            return False
