from threading import Thread

import xbmc

from resources.lib.api.open_subtitles import OpenSubtitles
from resources.lib.api.trakt_api import trakt
from resources.lib.const import SERVICE, SETTINGS, LANG, ICON, GENERAL, COLOR
from resources.lib.kodilogging import service_logger
from resources.lib.routing.router import router
from resources.lib.services import Service
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import SQLiteStorage
from resources.lib.subtitles import Subtitles
from resources.lib.utils.kodiutils import notification, get_icon, get_string, colorize

MONITOR_INTERVAL = 1


class PlayerService(Service):
    SERVICE_NAME = SERVICE.PLAYER_SERVICE

    def __init__(self, api, monitor_service):
        super(PlayerService, self).__init__()
        self.watched_time = 0
        self._api = api
        self._event_callbacks = {}
        self.db = SQLiteStorage.WatchHistory()
        self.trakt_id = None
        self.current_time = 0
        self.total_time = 0
        self.media_id = None
        self.monitor_service = monitor_service

    def clear(self):
        self.trakt_id = None
        self.media_id = None
        self.current_time = 0
        self.total_time = 0

    def monitor_start(self):
        self.update_progress()
        self.monitor_service.start(MONITOR_INTERVAL, self.monitor_start)

    def monitor_stop(self):
        self.monitor_service.stop()

    @staticmethod
    def get_subtitles(media, subtitles_string, container):
        Subtitles.get(media, subtitles_string, container)
        if len(container):
            lang = LANG.SUBTITLES_FOUND
            icon = ICON.SUBTITLES_FOUND
        else:
            lang = LANG.SUBTITLES_NOT_FOUND
            icon = ICON.SUBTITLES_NOT_FOUND
        notification("{0} | {1}".format(colorize(COLOR.LIGHTSKYBLUE, GENERAL.PLUGIN_ABBREVIATION), get_string(LANG.AUTOMATIC_SUBTITLES_SEARCH)),
                     get_string(lang), 5000,
                     get_icon(icon), False)
        return container

    @staticmethod
    def set_subtitles(media, subtitles_string):
        service_logger.debug('Subtitles are missing. Trying to find some.')
        subs = []
        PlayerService.get_subtitles(media, subtitles_string, subs)
        for sub in subs:
            xbmc.Player().setSubtitles(sub)

    def play(self, handle, item, url, media_id, trakt_id, media, sub_strings=None):
        self.media_id = media_id
        self.trakt_id = trakt_id

        if handle == -1:
            xbmc.Player().play(url, item)
        else:
            router.set_resolved_url(handle, item)
        service_logger.debug('Sending API request to increment play count')
        self._api.media_played(media_id)
        root_media = self._api.api_response_handler(self._api.media_detail(media_id))
        media_type = root_media.get('info_labels', {}).get('mediatype')
        if settings[SETTINGS.PLAYED_ITEMS_HISTORY]:
            self.db.add(media_id, media_type)

        service_logger.debug('PlayerService.media_played %r' % trakt_id)
        if sub_strings:
            self.set_subtitles(media, sub_strings)

    def update_progress(self):
        if not xbmc.Player().isPlayingVideo():
            return
        try:
            self.current_time = xbmc.Player().getTime()
            self.total_time = xbmc.Player().getTotalTime()
            service_logger.debug('PlayerService.updating_progress %s/%s' % (self.current_time, self.total_time))
        except Exception:
            pass

    def scrobble_start(self):
        service_logger.debug('PlayerService.scrobble_start: %s' % self.trakt_id)
        if self.trakt_id:
            trakt.scrobble_start(self.trakt_id, self.watchedPercent())

    def scrobble_pause(self):
        service_logger.debug('PlayerService.scrobble_pause')
        if self.trakt_id:
            trakt.scrobble_pause(self.trakt_id, self.watchedPercent())

    def scrobble_stop(self):
        service_logger.debug('PlayerService.scrobble_stop')
        if self.trakt_id:
            trakt.scrobble_stop(self.trakt_id, self.watchedPercent())

    def watchedPercent(self):
        self.update_progress()
        try:
            result = self.current_time / self.total_time * 100
        except ZeroDivisionError:
            result = 0
        # service_logger.debug('PlayerService.watchedPercent current: %d, total: %d, perecent: %d%%' % (self.current_time, self.total_time, result))
        return result
