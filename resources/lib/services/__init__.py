import json
import threading

import xbmc

from resources.lib.const import GENERAL
from resources.lib.kodilogging import service_logger


class Service(xbmc.Monitor):
    SERVICE_NAME = ''
    _event_callbacks = {}

    def onNotification(self, sender, method, data):
        if sender == GENERAL.PLUGIN_ID:
            command_info = json.loads(data)
            self.emit(command_info.get('command'), command_info.get('command_params'))

    def emit(self, service_event, params):
        service_logger.debug('Emitting service event %s' % service_event)
        return self._event_callbacks.get(service_event, lambda **kwargs: None)(**params)


class TimerService(Service):
    def __init__(self, immediate=False):
        super(TimerService, self).__init__()
        self.timer = None
        self.immediate = immediate

    def start(self, interval, fn):
        if self.immediate:
            self.immediate = False
            interval = 0
        self.timer = threading.Timer(interval, fn)
        self.timer.start()
        service_logger.debug('Timed service %s started. Executed in: %s' % (self.__class__.__name__, str(interval)))

    def stop(self):
        if self.timer:
            try:
                self.timer.cancel()
                self.timer.join()
                service_logger.debug('Timed service %s stopped' % self.__class__.__name__)
            except Exception as err:
                service_logger.error(err)


class ThreadService(Service):
    def __init__(self):
        super(ThreadService, self).__init__()
        self.threads = []

    def stop(self):
        for t in self.threads:
            t.join()
