from resources.lib.const import SETTINGS
from resources.lib.kodilogging import service_logger
from resources.lib.services import Service
from resources.lib.storage.settings import settings
from resources.lib.utils.kodiutils import refresh


# settings_actions = {
#     SETTINGS.USE_LIBRARY: library_service_control
# }


class SettingsService(Service):
    def __init__(self):
        super(SettingsService, self).__init__()
        self.settings = settings

    def onSettingsChanged(self):
        old_settings = self.settings.current_values.copy()
        self.settings.load()
        for k, v in old_settings.items():
            if v != self.settings.current_values[k]:
                service_logger.debug('Setting changed. Refreshing.')
                # settings_actions.get(k, lambda x: x)(v)
                refresh()
                break
