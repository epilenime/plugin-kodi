from resources.lib.kodilogging import service_logger
from resources.lib.players import BasePlayer
from resources.lib.utils.kodiutils import get_library_resume


class StreamCinemaPlayer(BasePlayer):
    def __init__(self, service, player_core=0):
        super(StreamCinemaPlayer, self).__init__(service, player_core)
        self._service = service
        self._playing = False

    def _check_if_playing_sc(self):
        self._playing = self._service.media_id is not None

    def _stop_playing(self):
        self._playing = False
        self._service.monitor_stop()
        self._service.scrobble_stop()

    def _playback_done(self):
        self._stop_playing()
        self._service.clear()

    def onAVStarted(self):
        if not self._playing:
            return
        service_logger.debug("onAVStarted")
        self.resume_library_item()
        self._service.scrobble_start()
        self._service.monitor_start()

    def resume_library_item(self):
        library_resume_pos = get_library_resume()
        if library_resume_pos:
            self.seekTime(library_resume_pos)

    def onPlayBackStarted(self):
        self._check_if_playing_sc()
        service_logger.debug("Playback started")

    def onPlayBackPaused(self):
        if not self._playing:
            return
        service_logger.debug("Playback paused")
        self._service.scrobble_pause()

    def onPlayBackResumed(self):
        if not self._playing:
            return
        service_logger.debug("Playback resumed")
        self._service.scrobble_start()

    def onPlayBackEnded(self):
        if not self._playing:
            return
        service_logger.debug("Playback ended")
        self._playback_done()

    def onPlayBackStopped(self):
        if not self._playing:
            return
        service_logger.debug("Playback stopped")
        self._playback_done()

    def onPlayBackSeek(self, time, offset):
        if not self._playing:
            return
        service_logger.debug("Playback seek time: %d, offset: %d" % (time, offset))
        self._service.scrobble_start()

    def onPlayBackSeekChapter(self, chapter):
        if not self._playing:
            return
        service_logger.debug("Playback seek chapter")
        self._service.scrobble_start()

