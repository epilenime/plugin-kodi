from resources.lib.const import GITLAB_ENDPOINT, URL
from resources.lib.storage.settings import settings
from resources.lib.wrappers.http import Http


class GitLabAPI:
    PRIVATE_TOKEN = '2cXgXEfhyy6P-5nN3tbb'
    PROJECT_ID = '2'

    @property
    def headers(self):
        common = settings.common_headers()
        common['PRIVATE-TOKEN'] = self.PRIVATE_TOKEN
        return common

    def _get(self, url):
        sanitized_url = url.strip('/')
        return Http.get(
            '{0}/{1}/'.format(URL.GITLAB_URL, sanitized_url),
            headers=self.headers,
            timeout=5
        )

    def get_latest_release(self):
        return self._get(GITLAB_ENDPOINT.RELEASES.format(project_id=self.PROJECT_ID)).json()
