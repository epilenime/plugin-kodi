import os
from threading import Thread

import xbmc
import xbmcvfs

from resources.lib.api.open_subtitles import OpenSubtitles
from resources.lib.api.titulky import Titulky
from resources.lib.const import LANG, SETTINGS, SUBTITLES_PROVIDER
from resources.lib.gui import MediaInfoRenderer
from resources.lib.storage.settings import settings
from resources.lib.utils.kodiutils import get_string


class Subtitles:
    def __init__(self):
        self.providers = {
            SUBTITLES_PROVIDER.OPENSUBTITLES: {
                'obj': OpenSubtitles,
                'search_string': MediaInfoRenderer.TITLE.subtitles_string,
                'cond': lambda: settings[SETTINGS.SUBTITLES_PROVIDER_ENABLE_OS_ORG] and settings[SETTINGS.SUBTITLES_PROVIDER_USERNAME] and settings[
                    SETTINGS.SUBTITLES_PROVIDER_PASSWORD]
            },
            SUBTITLES_PROVIDER.TITULKY: {
                'obj': Titulky,
                'search_string': Titulky.subtitles_string,
                'cond': lambda: settings[SETTINGS.SUBTITLES_PROVIDER_ENABLE_TITULKY_COM],
            },
        }

    def _get(self, media, search_string, provider, container):
        provider_obj = provider['obj']
        result = provider_obj.search(media, search_string[provider_obj.name])
        if result:
            content, lang = provider_obj.download(result)
            if content:
                subtitle = xbmc.translatePath('special://temp/')
                subtitle = os.path.join(subtitle,
                                        get_string(LANG.AUTO_SUBTITLES) + '.%s.%s.srt' % (lang, provider_obj.name))
                file = xbmcvfs.File(subtitle, 'w')
                file.write(str(content))
                file.close()
                container.append(subtitle)

    def get(self, media, search_string, container):
        threads = []
        for provider in list(self.providers.values()):
            if provider.get('cond', lambda *args: True)():
                t = Thread(target=self._get, args=(media, search_string, provider, container))
                t.start()
                threads.append(t)

        for t in threads:
            t.join()

    def build_search_string(self, labels):
        strings = {}
        for provider in list(self.providers.values()):
            s = provider['search_string'](labels)
            strings[provider['obj'].name] = s
        return strings

    def valid_credentials(self, provider, username, password):
        return self.providers[provider]['obj'].valid_credentials(username, password)

    def set_credentials(self, provider, username, password):
        return self.providers[provider]['obj'].set_credentials(username, password)




Subtitles = Subtitles()
